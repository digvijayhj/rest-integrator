package com.micro.services.restintegrator.restintegrator.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Data
@Configuration
@ConfigurationProperties(prefix = "rest-api")
@PropertySource(value = "classpath:rest-api-properties.yml", factory = YamlPropertySourceFactory.class)
public class RestApiProperties {

    private GoRest goRest;
    
    @Data
    public static class GoRest {
        private String uriDomain;
        private String uriUser;
        private String uriAuthToken;
    }
}
