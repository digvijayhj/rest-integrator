package com.micro.services.restintegrator.restintegrator.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReqBody {

    @JsonProperty("name")
    public String name;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("email")
    public String email;
    @JsonProperty("status")
    public String status;

}