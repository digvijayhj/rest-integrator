package com.micro.services.restintegrator.restintegrator.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorInfo {
    @JsonProperty("errorCode")
    private String errorCode;
    @JsonProperty("message")
    private String errorMsg;
}
