package com.micro.services.restintegrator.restintegrator;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.Properties;

@SpringBootApplication
public class RestIntegratorApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(RestIntegratorApplication.class).properties(getProperties()).build().run(args);
    }

    static Properties getProperties() {
        var properties = new Properties();
        properties.put("spring.config", "rest-api-properties.yml");
        return properties;
    }
}
