package com.micro.services.restintegrator.restintegrator.utility;

import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.SSLException;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

@Component("RestMethods")
public class RestConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestConsumer.class);

    /**
     * This method is used to make GET Rest API call
     *
     * @param uri
     * @param consumer
     * @param uriVariables
     * @param eClass
     * @param <E>
     * @return
     * @throws SSLException
     */
    public <E> E getRestEntity(final String uri, Consumer<HttpHeaders> consumer, final Map<String, Object> uriVariables,
                               final Class<E> eClass) throws SSLException {

        LOGGER.debug("Get request url: {}, parameters: {}", uri, uriVariables);

        E responseObj = null;

        var client = getWebClient();

        if (Objects.nonNull(consumer))
            responseObj = Objects.requireNonNull(client
                    .get()
                    .uri(uri, uriVariables)
                    .headers(consumer)
                    .retrieve()
                    .bodyToMono(eClass)
                    .block());
        // for default headers
        else
            responseObj = Objects.requireNonNull(client
                    .get()
                    .uri(uri, uriVariables)
                    .retrieve()
                    .bodyToMono(eClass)
                    .block());

        return responseObj;
    }

    /**
     * This method is used to make POST Rest API call
     *
     * @param uri
     * @param consumer
     * @param uriVariables
     * @param eClass
     * @param requestBody
     * @param <T>
     * @param <E>
     * @return
     * @throws SSLException
     */
    public <T, E> E postRestEntity(final String uri, Consumer<HttpHeaders> consumer, final Map<String, Object>
            uriVariables, final Class<E> eClass, final T requestBody) throws SSLException {

        LOGGER.debug("Post request url: {}, parameters: {}", uri, uriVariables);

        E responseObj = null;

        var client = getWebClient();

        if(Objects.nonNull(consumer))
            responseObj = Objects.requireNonNull(client
                    .post()
                    . uri(uri, uriVariables)
                    .headers(consumer)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );
        // for default headers
        else
            responseObj = Objects.requireNonNull(client
                    .post()
                    .uri(uri, uriVariables)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );

        return responseObj;
    }

    /**
     * This method is used to make PATCH Rest API call
     *
     * @param uri
     * @param consumer
     * @param uriVariables
     * @param eClass
     * @param requestBody
     * @param <E>
     * @param <T>
     * @return
     * @throws SSLException
     */
    public <E, T> E patchRestEntity(final String uri, Consumer<HttpHeaders> consumer, final Map<String, Object>
            uriVariables, final Class<E> eClass, final T requestBody) throws SSLException {

        LOGGER.debug("Patch request url: {}, parameters: {}", uri, uriVariables);

        E responseObj = null;

        var client = getWebClient();

        if(Objects.nonNull(consumer))
            responseObj = Objects.requireNonNull(client
                    .patch()
                    .uri(uri, uriVariables)
                    .headers(consumer)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );
        // for default headers
        else
            responseObj = Objects.requireNonNull(client
                    .patch()
                    .uri(uri, uriVariables)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );

        return responseObj;
    }

    /**
     * This method is used to make PUT Rest API call
     *
     * @param uri
     * @param consumer
     * @param uriVariables
     * @param eClass
     * @param requestBody
     * @param <E>
     * @param <T>
     * @return
     * @throws SSLException
     */
    public <E, T> E putRestEntity(final String uri, Consumer<HttpHeaders> consumer, final Map<String, Object>
            uriVariables, final Class<E> eClass, final T requestBody) throws SSLException {

        LOGGER.debug("Put request url: {}, parameters: {}", uri, uriVariables);

        E responseObj = null;

        var client = getWebClient();

        if(Objects.nonNull(consumer))
            responseObj = Objects.requireNonNull(client
                    .put()
                    .uri(uri, uriVariables)
                    .headers(consumer)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );
            // for default headers
        else
            responseObj = Objects.requireNonNull(client
                    .put()
                    .uri(uri, uriVariables)
                    .body(Mono.just(requestBody), requestBody.getClass())
                    .retrieve()
                    .bodyToMono(eClass)
                    .block()
            );

        return responseObj;

    }

    /**
     * This method is used to make DELETE Rest API call
     *
     * @param uri
     * @param consumer
     * @param uriVariables
     * @param eClass
     * @param <E>
     * @param <T>
     * @return
     * @throws SSLException
     */
    public <E> E deleteRestEntity(final String uri, Consumer<HttpHeaders> consumer, final Map<String, Object>
            uriVariables, final Class<E> eClass) throws SSLException {

        LOGGER.debug("Delete request url: {}, parameters: {}", uri, uriVariables);

        E responseObj = null;

        var client = getWebClient();

        if (Objects.nonNull(consumer))
            responseObj = (E) Objects.requireNonNull(client
                    .delete()
                    .uri(uri, uriVariables)
                    .headers(consumer)
                    .retrieve()
                    .bodyToMono(eClass)
            );
            // for default headers
        else
            responseObj = (E) Objects.requireNonNull(client
                    .delete()
                    .uri(uri, uriVariables)
                    .retrieve()
                    .bodyToMono(eClass)
            );

        return responseObj;
    }
    /**
     * Method used to initialize WebClient instance.
     *
     * @param <E>
     * @return WebClient
     * @throws SSLException
     */
    private <E> WebClient getWebClient() throws SSLException {
        var sslContext = SslContextBuilder.forClient()
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
        var httpClient = HttpClient
                .create()
                .secure(sslContextSpec -> sslContextSpec.sslContext(sslContext));
        var httpConnector = new ReactorClientHttpConnector(httpClient);
        return WebClient
                .builder()
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .clientConnector(httpConnector)
                .build();
    }
}

