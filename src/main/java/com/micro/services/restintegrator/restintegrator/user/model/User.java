package com.micro.services.restintegrator.restintegrator.user.model;

import com.fasterxml.jackson.annotation.*;
import com.micro.services.restintegrator.restintegrator.generic.AbstractInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode(callSuper = true)
public class User extends AbstractInfo {

    @JsonProperty("data")
    private List<UserData> userData;
}
