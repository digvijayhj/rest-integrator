package com.micro.services.restintegrator.restintegrator.user;

import com.micro.services.restintegrator.restintegrator.user.model.ReqBody;
import com.micro.services.restintegrator.restintegrator.user.model.User;
import com.micro.services.restintegrator.restintegrator.user.model.UserModel;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private RestUser restUser;

    @GetMapping("/list")
    public User getUserList() {
        return restUser.getUserListResponse();
    }

    @PostMapping("/create")
    public UserModel createNewUserRecord(@NonNull @RequestBody ReqBody requestBody) {
        return restUser.createNewUser(requestBody);
    }

    @PatchMapping("/update/{id}")
    public UserModel updateUserRecord(@PathVariable String id, @NonNull @RequestBody ReqBody requestBody) {
        return restUser.updateUserDetails(requestBody, id);
    }

    @DeleteMapping("/delete/{id}")
    public UserModel deleteUserRecord(@PathVariable String id) {
        return restUser.deleteSpecificUserRecord(id);
    }
}

