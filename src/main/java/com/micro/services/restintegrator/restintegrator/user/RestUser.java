package com.micro.services.restintegrator.restintegrator.user;

import com.micro.services.restintegrator.restintegrator.user.model.ReqBody;
import com.micro.services.restintegrator.restintegrator.user.model.User;
import com.micro.services.restintegrator.restintegrator.user.model.UserModel;

public interface RestUser {

    /**
     * This method is used to fetch list of Users
     *
     * @return User
     */
    User getUserListResponse();

    /**
     * This method is used to create new User record
     *
     * @param requestBody
     * @return UserModel
     */
    UserModel createNewUser(ReqBody requestBody);

    /**
     * This method is used to update existing User record
     *
     * @param requestBody
     * @return
     */
    UserModel updateUserDetails(ReqBody requestBody, String id);

    /**
     * This method is used to create new User record
     *
     * @param id
     * @return UserModel
     */
    UserModel deleteSpecificUserRecord(String id);
}
