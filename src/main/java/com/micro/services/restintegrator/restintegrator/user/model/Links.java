package com.micro.services.restintegrator.restintegrator.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {

    @JsonProperty("previous")
    private String previous;
    @JsonProperty("current")
    private String current;
    @JsonProperty("next")
    private String next;
}
