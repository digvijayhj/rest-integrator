package com.micro.services.restintegrator.restintegrator.generic;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.micro.services.restintegrator.restintegrator.user.model.Pagination;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MetaData {
    @JsonProperty("pagination")
    private Pagination pagination;
}
