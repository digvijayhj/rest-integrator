package com.micro.services.restintegrator.restintegrator.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagination {

    @JsonProperty("total")
    private String total;
    @JsonProperty("pages")
    private String pages;
    @JsonProperty("page")
    private String page;
    @JsonProperty("limit")
    private String limit;
    @JsonProperty("links")
    private Links links;
}
