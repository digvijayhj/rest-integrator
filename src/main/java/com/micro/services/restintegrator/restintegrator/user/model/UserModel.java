package com.micro.services.restintegrator.restintegrator.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.micro.services.restintegrator.restintegrator.generic.AbstractInfo;
import com.micro.services.restintegrator.restintegrator.generic.MetaData;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel extends AbstractInfo {

    @JsonProperty("meta")
    public MetaData meta;
    @JsonProperty("data")
    public UserData data;
    @JsonProperty("message")
    public String message;
}
