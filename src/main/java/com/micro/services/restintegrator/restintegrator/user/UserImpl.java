package com.micro.services.restintegrator.restintegrator.user;

import com.micro.services.restintegrator.restintegrator.config.RestApiProperties;
import com.micro.services.restintegrator.restintegrator.generic.ErrorInfo;
import com.micro.services.restintegrator.restintegrator.user.model.ReqBody;
import com.micro.services.restintegrator.restintegrator.user.model.User;
import com.micro.services.restintegrator.restintegrator.user.model.UserModel;
import com.micro.services.restintegrator.restintegrator.utility.RestConsumer;
import com.micro.services.restintegrator.restintegrator.utility.RestLiterals;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * Class used to consume rest api calls for user details
 *
 * @author digvijay
 */
@Service
public class UserImpl implements RestUser {

    private static final String MISSING_PROPERTIES = "Failed to fetch api properties.";

    @Autowired
    private RestApiProperties apiProperties;

    @Autowired
    private RestConsumer restConsumer;

    @Override
    public User getUserListResponse() {

        var response = new User();

        // Check for missing api properties.
        var errorInfo = validateProperties(new ErrorInfo());
        if(StringUtils.hasText(errorInfo.getErrorMsg())) {
            response.setError(errorInfo);
            return response;
        }

        try {
            var headers = setUserHeaders();
            var uri = apiProperties.getGoRest().getUriDomain() + apiProperties.getGoRest().getUriUser();

            response = restConsumer.getRestEntity(uri, headers, new HashMap<>(), User.class);

        } catch (Exception e) {
            var errResponse = new User();
            var error = new ErrorInfo();
            error.setErrorMsg("Failed to fetch User List of Response.");
            errResponse.setError(error);
            return errResponse;
        }
        return response;
    }

    @Override
    public UserModel createNewUser(ReqBody requestBody) {
        var response = new UserModel();

        // Check for missing api properties.
        var errorInfo = validateProperties(new ErrorInfo());
        if(StringUtils.hasText(errorInfo.getErrorMsg())) {
            response.setError(errorInfo);
            return response;
        }

        try {
            var headers = setUserHeaders();
            var uri = apiProperties.getGoRest().getUriDomain() + apiProperties.getGoRest().getUriUser();

            response = restConsumer.postRestEntity(uri, headers, new HashMap<>(), UserModel.class, requestBody);

        } catch (Exception e) {
            var errResponse = new UserModel();
            var error = new ErrorInfo();
            error.setErrorMsg("Failed to Create User record.");
            errResponse.setError(error);
            return errResponse;
        }
        return response;
    }

    @Override
    public UserModel updateUserDetails(ReqBody requestBody, String id) {
        var response = new UserModel();

        // Check for missing api properties.
        var errorInfo = validateProperties(new ErrorInfo());
        if(StringUtils.hasText(errorInfo.getErrorMsg())) {
            response.setError(errorInfo);
            return response;
        }

        try {
            var headers = setUserHeaders();
            var uri = apiProperties.getGoRest().getUriDomain() + apiProperties.getGoRest().getUriUser() +
                    RestLiterals.SPECIFIC_USER;
            var uriVariables = new HashMap<String, Object>();
            uriVariables.put("id", id);
            response = restConsumer.patchRestEntity(uri, headers, uriVariables, UserModel.class, requestBody);

        } catch (Exception e) {
            var errResponse = new UserModel();
            var error = new ErrorInfo();
            error.setErrorMsg("Failed to Update User record.");
            errResponse.setError(error);
            return errResponse;
        }
        return response;
    }

    @Override
    public UserModel deleteSpecificUserRecord(String id) {
        var response = new UserModel();

        // Check for missing api properties.
        var errorInfo = validateProperties(new ErrorInfo());
        if(StringUtils.hasText(errorInfo.getErrorMsg())) {
            response.setError(errorInfo);
            return response;
        }

        try {
            var headers = setUserHeaders();
            var uri = apiProperties.getGoRest().getUriDomain() + apiProperties.getGoRest().getUriUser() +
                    RestLiterals.SPECIFIC_USER;
            var uriVariables = new HashMap<String, Object>();
            uriVariables.put("id", id);
            restConsumer.deleteRestEntity(uri, headers, uriVariables, Void.class);
            response.setMessage("Record Deleted Successfully.");
        } catch (Exception e) {
            var errResponse = new UserModel();
            var error = new ErrorInfo();
            error.setErrorMsg("Failed to Delete User record.");
            errResponse.setError(error);
            return errResponse;
        }
        return response;
    }

    /**
     * Method used to check api properties are available or not.
     *
     * @param errorInfo
     * @return
     */
    private ErrorInfo validateProperties(ErrorInfo errorInfo) {
        if(Objects.isNull(apiProperties.getGoRest()) || !StringUtils.hasText(apiProperties.getGoRest().getUriDomain())
                || !StringUtils.hasText(apiProperties.getGoRest().getUriUser()) || !StringUtils.hasText(apiProperties.getGoRest().getUriAuthToken())) {
            errorInfo.setErrorMsg(MISSING_PROPERTIES);
            return errorInfo;
        }
        return errorInfo;
    }

    /**
     * Method used to add http headers
     *
     * @return httpHeaders
     */
    private Consumer<HttpHeaders> setUserHeaders() {
        var headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", "Bearer " + apiProperties.getGoRest().getUriAuthToken());
        return e -> e.addAll(headers);
    }
}
